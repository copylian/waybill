<?php
require __DIR__ . '/vendor/autoload.php';
use Waybill\Report;

const baseurl = "http://116.182.4.67:50065/platform"; // 测试环境-信息上报接口
//const baseurl = "http://116.182.4.67:50033/platform"; // 正式环境-信息上报接口
const account =  "13459253256"; // 账号
const pwd = "yunDing99@56cc"; // 密码

/**
 * 打印输出方法
 * @param $data
 * @return void
 */
function p($data = []): void
{
  if (is_array($data) || is_object($data)) {
    echo "<pre>";
    print_r($data);
    echo "</pre>\n";
  } else {
    echo $data, PHP_EOL;
  }
}

/**
 * 处理函数
 * @param $type
 * @return void
 */
function deal($type = "", $config = [])
{
  //实例化类
  $waybill = new Report($config);
  switch ($type) {
    case "uploadFile":
      //上传电子运单、车辆信息、驾驶员信息中相关图片文件(备注:文件大小需要小于2M)
      $reqData = [
        "file" => "E:\\www\\test\\a.jpg" // 文件,必填
//        "file" => "E:\\www\\test\\token_wb.json" // 文件,必填
      ];
      $res = $waybill->dealReport("uploadFile", $reqData);
      if($res["code"] == 1){
        //记录日志
        p($res["data"]["result"]); // 6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg、b7edacc0-79b3-443d-b2e5-93bdd26a971b.jpg
      } else {
        //记录日志
        p($res);
      }
      break;
    case "transport":
      //上报电子运单数据前，必须先上报通过校验的车辆数据和驾驶员数据
      //请求数据
      $reqData = [
        [
          "originalDocumentNumber" => "NO20230325000333", // 原始单号
          "shippingNoteNumber" => "NO20230325000333", // 运单号,必填
          "serialNumber" => "0000", // 分段分单号,必填
          "vehicleAmount" => 1, // 运输总车辆数
          "transportTypeCode" => 1, // 运输组织类型代码：1-公路运输、2-公铁联运、3-公水联运、4-公空联运、5-公铁水联运、6-公铁空联运、7-公水空联运、8-公铁水空联运
          "sendToProDateTime" => "2022-12-07 16:22:03", // 运单上传时间,yyyy-MM-dd HH:mm:ss
          "carrier" => "黑龙江云鼎物联科技有限公司", // 网络货运经营者名称
          "unifiedSocialCreditIdentifier" => "99910109MA1G566Y32", // 统一社会信用代码,必填
          "permitNumber" => "510114118554", // 道路运输经营许可证编号
          "consignmentDateTime" => "2022-12-07 16:17:59", // 运单生成时间,yyyy-MM-dd HH:mm:ss
          "businessTypeCode" => "1002996", // 业务类型代码：1002996-干线普货运输、1003997-城市配送、1003998-农村配送、1002998-集装箱运输、1003999-其他
          "despatchActualDateTime" => "2022-12-07 16:19:30", // 发货日期时间,yyyy-MM-dd HH:mm:ss
          "goodsReceiptDateTime" => "2022-12-07 16:19:37", // 收货日期时间,yyyy-MM-dd HH:mm:ss
          "consignorInfoQuery" => [ // 托运人信息
            "consignor" => "张三", // 托运人名称
            "consignorId" => "99910109MA1G566Y32", // 托运人统一社会信代码或个人证件号
            "placeOfLoading" => "北京市朝阳区安华西里8号楼", // 装货地址
            "countrySubdivisionCode" => "110105" // 装货地点的国家行政区划代码或国别代码
          ],
          "consigneeInfoQuery" => [ // 收货方信息
            "consignee" => "张彦", // 收货方名称
            "consigneeId" => "99910109991G566Y33", // 收货方统一社会信用代码或个人证件号码
            "goodsReceiptPlace" => "北京市朝阳区安华西里88号楼", // 收货地址
            "countrySubdivisionCode" => "110105", // 收货地点的国家行政区划代码或国别代码
            "totalMonetaryAmount" => 100000 // 运费金额,保留三位小数,传入*1000 必填，货币单位为人民币（元）
          ],
          "vehicleInfoQuery" => [ // 车辆信息,必填
            "vehiclePlateColorCode" => 1, // 车牌颜色代码,必填
            "vehicleNumber" => "闽A1234562", // 车辆牌照号,必填
            "despatchActualDateTime" => "2022-12-07 16:19:30",// 发货日期时间,yyyy-MM-dd HH:mm:ss
            "goodsReceiptDateTime" => "2022-12-07 16:19:37", // 收货日期时间,yyyy-MM-dd HH:mm:ss
            "placeOfLoading" => "北京", // 装货地址
            "loadingCountrySubdivisionCode" => "110105", // 装货地址的国家行政区划代码或国别代码
            "goodsReceiptPlace" => "北京", // 收货地址
            "receiptCountrySubdivisionCode" => "110105", // 收货地址的国家行政区划代码或国别代码
            "driverLoadDateTime" => "2022-12-07 16:19:31", // 驾驶员装货地定位的上报时间,yyyy-MM-dd HH:mm:ss
            "driverLoadLonlat" => "12132123,1212312", // 驾驶员装货地定位的经度纬度,英文逗号分隔
            "driverUnloadDateTime" => "2022-12-07 16:19:36", // 驾驶员卸货地定位的上报时间,yyyy-MM-dd HH:mm:ss
            "driverUnloadLonlat" => "12132123,1212312" // 驾驶员卸货地定位的经度纬度
          ],
          "drivers" => [ // 驾驶员信息列表
            [
              "driverName" => "张三", // 姓名,必填
              "drivingLicense" => "513029199908093864" // 身份证号,必填
            ]
          ],
          "goodsInfos" => [ // 货物信息列表
            [
              "descriptionOfGoods" => "管件2", // 货物名称,必填
              "cargoTypeClassificationCode" => "0600", // 货物类型分类代码,必填：0100-煤炭及制品、0200-石油、天然气及制品、0300-金属矿石、0400-钢铁、0500-矿建材料、0600-水泥、0700-木材、0800-非金属矿石、0900-化肥及农药、1000-盐、1100-粮食、1200-机械、设备、电器、1300-轻工原料及制品、1400-有色金属、1500-轻工医药产品、1601-鲜活农产品、1602-冷藏冷冻货物、1701-商品汽车、1700-其他
              "goodsItemGrossWeight" => 99000, // 货物项毛重,保留三位小数,传入*1000 重量单位以 KGM 千克填写数值
              "cube" => 10000, // 体积,保留四位小数,传入*10000,体积单位以 DMQ 立方米填写数值
              "totalNumberOfPackages" => 1 // 总件数
            ]
          ],
          "actualCarrierInfo" => [ // 实际承运人信息
            "actualCarrierName" => "zs", // 实际承运人名称
            "actualCarrierBusinessLicense" => "aaa", // 实际承运人道路运输经营许可证号
            "actualCarrierId" => "bbb" // 实际承运人统一社会信用代码或证件号码
          ],
          "insuranceInformation" => [ // 保险信息：电子运单数据中的保险信息必填，若未投保，保险单号和保险公司代码填 none
            "policyNumber" => "BX1235554", // 保险单号
            "insuranceCompanyCode" => "ABIC" // 保险公司代码：ABIC-安邦财产保险股份有限公司、AICS-永诚财产保险股份有限公司、BOCI-中银保险有限公司、BPIC-渤海财产保险股份有限公司、CAIC-长安责任保险股份有限公司、CCIC-中国大地财产保险股份有限公司、CICP-中华联合财产保险公司、CPIC-中国太平洋财产保险股份有限公司、DBIC-都邦财产保险股份有限公司、GPIC-中国人寿财产保险公司、HAIC-华安财产保险股份有限公司、HTIC-华泰财产保险股份有限公司、MACN-民安保险(中国)有限公司、PAIC-中国平安财产保险股份有限公司、PICC-中国人民财产保险股份有限公司、TAIC-天安保险股份有限公司、TPIC-太平保险有限公司、YDCX-英大泰和人寿保险股份有限公司、YGBX-阳光财产保险股份有限公司、ZKIC-紫金财产保险公司、MACN-民安保险（中国）有限公司、YAIC-永安财产保险股份有限公司、TPBX-天平保险公司、ACIC-安诚财产保险股份有限公司、DHIC-鼎和财产保险股份有限公司、ALIC-安联保险公司、QITA-其他保险公司
          ],
          "remark" => "运单", // 备注
          "file" => ["6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg","6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg"], // 合同文件/照片
          "ownersContractFile" => ["6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg","6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg"], // 货主合同,必填
          "loadGoodsImages" => ["6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg","6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg"], // 装货照片
          "unLoadGoodsImages" => ["6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg","6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg"], // 卸货照片
          "backDocumentImages" => ["6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg","6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg"], // 回单照片
          "appTrackFile" => "2a8f7aba-26f8-4342-a784-6df8243648a7.json", // APP轨迹的上传文件
          "sdkTrackFile" => "2a8f7aba-26f8-4342-a784-6df8243648a7.json" // SDK轨迹的上传文件
        ]
      ];
      $res = $waybill->dealReport("transport", $reqData);
      if($res["code"] == 1){
        p($res["data"]["result"]); // 数据上报成功,数据完整，等待系统校验
      } else {
        //记录日志
        p($res);
      }
      break;
    case "money":
      //请求数据
      $reqData = [
        [
          "documentNumber" => "62552521", // 单证号,必填
          "sendToProDateTime" => "2019-08-08 11:22:33",// 资金流水单上传时间,必填
          "carrier" => "天地轮公司", // 实际承运人名称,必填
          "actualCarrierId" => "4564s6af5d4f6s5", // 实际承运人统一社会信用代码或证件号码,必填
          "vehicleNumber" => "京A123456", // 车辆牌照号,必填
          "vehiclePlateColorCode" => 1, // 车牌颜色代码,必填
          "shippingNoteList" => [ // 运单列表,必填
            [
              "shippingNoteNumber" => "513029199908093863", // 运单号,必填
              "serialNumber" => "0000", // 分段分单号,必填
              "totalMonetaryAmount" => "2000000", // 总金额,必填,保留三位小数,传入*1000 该笔运输实际发生费用，含燃油、路桥费和 实际支付金额。人民币（元）
              "fuelAmount" => "100000", // 燃油费用,必填,保留三位小数,传入*1000货币单位为人民币（元）
              "etcAmount" => "50000" // etc费用,保留三位小数,传入*1000货币单位为人民币（元）
            ]
          ],
          "financialList" => [ // 财务列表,必填
            [
              "shippingNoteNumber" => "513029199908093863", // 托运单号,必填
              "serialNumber" => "0000", // 分段分单号,必填
              "driverName" => "张三", // 司机姓名,表示运费归属人,必填
              "driverLicense" => "02182942389423985734", // 司机身份证号,表示运费归属人,必填
              "paymentMeansCode" => "1", // 付款方式代码,必填：1-信用证、2 托收、3-汇付、31-银行汇票、32-银行转账、4、第三方支付平台、41-支付宝支付、42-微信支付、9-其他电子支付方式（不允许现金支付）
              "recipient" => "阿卡丽", // 收款方名称,必填
              "sex" => 1, // 收款人性别(1男 2女),必填
              "birthday" => "2021-10-10", // 收款人生日yyyy-MM-dd HH:mm:ss,必填
              "idcardNumber" => "123123918341213124", // 收款人身份证号,必填
              "phone" => "13419238203", // 收款人电话,必填
              "address" => "河北北京市长安区罗家坟1号", // 收款人地址,身份证上的地址,必填
              "receiptAccount" => "******拉斯柯达", // 收款帐户信息,必填
              "bankCode" => "阿卡丽", // 收款方银行代码,必填
              "networkName" => "北京某某银行", // 收款方银行卡银行网点名称
              "sequenceCode" => "32093298749853", // 流水号/序列号,必填
              "monetaryAmount" => "1800000", // 实际支付金额,保留三位小数,传入*1000 货币单位为人民币,必填
              "dateTime" => "2021-10-10 10:00:12", // 资金流水实际发生时间,yyyy-MM-dd HH:mm:ss,必填
              "payFile" => "32093298749853.jpg", // 支付凭证文件 (文件路径),必填
              "recipientIdCardBackImg" => "3209329dff87423429853.jpg", // 收款人身份证(副页),必填
              "recipientIdCardFrontImg" => "32093298742342349853.jpg" // 收款人身份证(正页),必填
            ]
          ],
          "remark" => "123" // 备注
        ]
      ];
      $res = $waybill->dealReport("money", $reqData);
      if($res["code"] == 1){
        p($res["data"]["result"]); // 数据上报成功,数据完整，等待系统校验
      } else {
        //记录日志
        p($res);
      }
      break;
    case "vehicle":
      //请求数据
      $reqData = [
        [
          "vehicleNumber" => "闽A1234562", // 车辆牌照号,必填
          "vehiclePlateColorCode" => 1, // 车牌颜色代码,必填：1-蓝色、4-白色、2-黄色、5-绿色、3-黑色、9-其他、91-农黄色、92-农绿色、93-黄绿色、94-渐变绿
          "vehicleType" => "H11", // 车辆类型代码,必填
          "owner" => "中交", // 所有人,必填
          "useCharacter" => "公用", // 使用性质
          "vin" => "111122223333", // 车辆识别代号,必填
          "issuingOrganizations" => "四川成都公安局", // 发证机关
          "vehicleLength" => 1000, // 车长 保留整数 默认单位毫米,必填
          "vehicleWidth" => 1222, // 车宽 保留整数 默认单位毫米
          "vehicleHeight" => 2552, // 车高 保留整数 默认单位毫米
          "registerDate" => "2021-04-23", // 注册日期,必填,yyyy-MM-dd
          "issueDate" => "2021-08-13", // 发证日期,必填yyyy-MM-dd
          "vehicleEnergyType" => "A", // 车辆能源类型,必填：A-汽油、B-柴油、C-电,以电能驱动的汽车、D-混合油、E-天然气、F-液化石油气、L-甲醇、M-乙醇、N-太阳能、O-混合动力、Y-无,仅限全挂车等无动力的、Z-其他
          "vehicleTonnage" => 400, // 核定载质量,必填,保留两位小数，传入*100 默认单位：吨
          "grossMass" => 600, // 吨位,必填,保留两位小数，传入*100 默认单位：吨
          "roadTransportCertificateNumber" => "123654789", // 道路运输证号,必填
          "trailerVehiclePlateNumber" => "123456789", // 挂车牌照号
          "remark" => "这是一辆不错的车", // 备注
          "licenseValidPeriodTo" => "2037-04-23", // 行驶证有效期至,必填,yyyy-MM-dd
          "transportValidPeriodTo" => "2037-04-23", // 道路运输证有效期至,yyyy-MM-dd
          "vehicleLicenseFirstSheetImg" => "6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg", // 行驶证照片（正页）,必填
          "vehicleLicenseSecondSheetImg" => "b7edacc0-79b3-443d-b2e5-93bdd26a971b.jpg", // 行驶证照片（副页）,必填
          "vehicleLicenseSecondSheetBackImg" => "6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg", // 行驶证照片（副页反面）
          "transportLicenseFirstSheetImg" => "b7edacc0-79b3-443d-b2e5-93bdd26a971b.jpg", // 道路运输证照片（正页）
          "transportLicenseSecondSheetImg" => "6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg", // 道路运输证照片（副页）
          "vehicleDriverImg" => "b7edacc0-79b3-443d-b2e5-93bdd26a971b.jpg" // 人车合影照片
        ],
        [
          "vehicleNumber" => "闽J1234563", // 车辆牌照号,必填
          "vehiclePlateColorCode" => 2, // 车牌颜色代码,必填：1-蓝色、4-白色、2-黄色、5-绿色、3-黑色、9-其他、91-农黄色、92-农绿色、93-黄绿色、94-渐变绿
          "vehicleType" => "H12", // 车辆类型代码,必填
          "owner" => "中交", // 所有人,必填
          "useCharacter" => "公用", // 使用性质
          "vin" => "111122223333", // 车辆识别代号,必填
          "issuingOrganizations" => "四川成都公安局", // 发证机关
          "vehicleLength" => 1000, // 车长 保留整数 默认单位毫米,必填
          "vehicleWidth" => 1222, // 车宽 保留整数 默认单位毫米
          "vehicleHeight" => 2552, // 车高 保留整数 默认单位毫米
          "registerDate" => "2021-04-23", // 注册日期,必填,yyyy-MM-dd
          "issueDate" => "2021-08-13", // 发证日期,必填yyyy-MM-dd
          "vehicleEnergyType" => "B", // 车辆能源类型,必填：A-汽油、B-柴油、C-电,以电能驱动的汽车、D-混合油、E-天然气、F-液化石油气、L-甲醇、M-乙醇、N-太阳能、O-混合动力、Y-无,仅限全挂车等无动力的、Z-其他
          "vehicleTonnage" => 400, // 核定载质量,必填,保留两位小数，传入*100 默认单位：吨
          "grossMass" => 600, // 吨位,必填,保留两位小数，传入*100 默认单位：吨
          "roadTransportCertificateNumber" => "123654789", // 道路运输证号,必填
          "trailerVehiclePlateNumber" => "123456789", // 挂车牌照号
          "remark" => "这是一辆不错的车", // 备注
          "licenseValidPeriodTo" => "2037-04-23", // 行驶证有效期至,必填,yyyy-MM-dd
          "transportValidPeriodTo" => "2037-04-23", // 道路运输证有效期至,yyyy-MM-dd
          "vehicleLicenseFirstSheetImg" => "6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg", // 行驶证照片（正页）,必填
          "vehicleLicenseSecondSheetImg" => "b7edacc0-79b3-443d-b2e5-93bdd26a971b.jpg", // 行驶证照片（副页）,必填
          "vehicleLicenseSecondSheetBackImg" => "6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg", // 行驶证照片（副页反面）
          "transportLicenseFirstSheetImg" => "b7edacc0-79b3-443d-b2e5-93bdd26a971b.jpg", // 道路运输证照片（正页）
          "transportLicenseSecondSheetImg" => "6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg", // 道路运输证照片（副页）
          "vehicleDriverImg" => "b7edacc0-79b3-443d-b2e5-93bdd26a971b.jpg" // 人车合影照片
        ]
      ];
      $res = $waybill->dealReport("vehicle", $reqData);
      if($res["code"] == 1){
        p($res["data"]["result"]); // 数据上报成功,数据完整，等待系统校验
      } else {
        //记录日志
        p($res);
      }
      break;
    case "driver":
      //请求数据
      $reqData = [
        [
          "driverName" => "张三", // 姓名,必填
          "drivingLicense" => "513029199908093863", // 身份证号,必填
          "vehicleClass" => "奥迪A5", // 准驾车型,必填
          "issuingOrganizations" => "成都", // 驾驶证发证机关,必填
          "validPeriodFrom" => "2021-04-23", // 驾驶证有效期自,必填,yyyy-MM-dd
          "validPeriodTo" => "2030-04-23", // 驾驶证有效期至,必填,yyyy-MM-dd
          "qualificationCertificate" => "驾驶资格证书", // 从业资格证号,必填
          "telephone" => "17584856412", // 手机号码,必填
          "remark" => "这是一个合格的驾驶员", // 备注,必填
          "idCardFrontImg" => "6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg", // 身份证照片(人像面),必填
          "idCardBackImg" => "6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg", // 身份证照片(国徽面),必填
          "driverLicenseFirstSheetImg" => "6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg", // 驾驶证照片(正页),必填
          "driverLicenseSecondSheetImg" => "6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg", // 驾驶证照片(副页)
          "transportCertificationImg" => "6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg", // 道路运输从业资格证照片
          "idCardValidPeriodTo" => "2037-04-23", // 身份证有效期至,必填,yyyy-MM-dd
          "transportCertificationValidPeriodTo" => "2021-04-23", // 从业资格证有效期至,yyyy-MM-dd
          "driverSex" => 1, // 驾驶员性别,必填：1-男、2-女
          "driverBirthday" => "2021-04-23" // 驾驶员生日,必填,yyyy-MM-dd
        ],
        [
          "driverName" => "飓风呀", // 姓名,必填
          "drivingLicense" => "513029199908093864", // 身份证号,必填
          "vehicleClass" => "奥迪A5", // 准驾车型,必填
          "issuingOrganizations" => "成都", // 驾驶证发证机关,必填
          "validPeriodFrom" => "2021-04-23", // 驾驶证有效期自,必填,yyyy-MM-dd
          "validPeriodTo" => "2036-04-23", // 驾驶证有效期至,必填,yyyy-MM-dd
          "qualificationCertificate" => "驾驶资格证书", // 从业资格证号,必填
          "telephone" => "17584856412", // 手机号码,必填
          "remark" => "这是一个合格的驾驶员", // 备注,必填
          "idCardFrontImg" => "6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg", // 身份证照片(人像面),必填
          "idCardBackImg" => "6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg", // 身份证照片(国徽面),必填
          "driverLicenseFirstSheetImg" => "6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg", // 驾驶证照片(正页),必填
          "driverLicenseSecondSheetImg" => "6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg", // 驾驶证照片(副页)
          "transportCertificationImg" => "6d04b614-2484-40f6-8a36-ebd9ed595b00.jpg", // 道路运输从业资格证照片
          "idCardValidPeriodTo" => "2030-04-23", // 身份证有效期至,必填,yyyy-MM-dd
          "transportCertificationValidPeriodTo" => "2021-04-23", // 从业资格证有效期至,yyyy-MM-dd
          "driverSex" => 1, // 驾驶员性别,必填：1-男、2-女
          "driverBirthday" => "2021-04-23" // 驾驶员生日,必填,yyyy-MM-dd
        ]
      ];
      $res = $waybill->dealReport("driver", $reqData);
      if($res["code"] == 1){
        p($res["data"]["result"]); // 数据上报成功,数据完整，等待系统校验
      } else {
        //记录日志
        p($res);
      }
      break;
    case "transportValidate":
      //请求数据
      $reqData = [
        [
          "shippingNoteNumber" => "NO20221207000026", // 运单号,必填
          "serialNumber" => "0000" // 分段分单号,必填
        ],
        [
          "shippingNoteNumber" => "NO20230325000333", // 运单号,必填
          "serialNumber" => "0000" // 分段分单号,必填
        ]
      ];
      $res = $waybill->dealReport("transportValidate", $reqData);
      if($res["code"] == 1){
        p($res["data"]["result"]);

//        $res["data"]["result"] = [
//          [
//            "id" => "4674136221283876966", // ID
//            "shippingNoteNumber" => "NO20221207000026", // 运单号
//            "serialNumber" => "0000", // 分段分单号
//            "validateStatus" => "通过校验", // 校验状态 通过，异常，待校验
//            "validateResult" => [ // 校验结果信息详情
//              [
//                "message" => "运单货物的总重量必须小于车辆核定载质量", // 校验规则信息
//                "flag" => "通过校验" // 校验规则状态 异常/通过校验
//              ],
//              [
//                "message" => "同一辆驾驶员在同一个时间段内只能执行一个运单",
//                "flag" => "通过校验"
//              ]
//            ],
//            "vehicleValidate" => [
//              "transId" => "4674136221283876966",
//              "vehicleNumber" => "沪EP1601",
//              "vehiclePlateColorCode" => "通过",
//              "validateResult" => [
//                [
//                  "message" => "上报的《行驶证》必须未过期",
//                  "flag" => "通过校验"
//                ]
//              ]
//            ],
//            "transDriver" => [
//              [
//                "transportId" => "4674136221283876966",
//                "driverName" => "贺雅琴",
//                "drivingLicense" => "430521198406040287",
//                "validateStatus" => "通过",
//                "validateResult" => [
//                  [
//                    "message" => "上报的《行驶证》必须未过期",
//                    "flag" => "通过校验"
//                  ]
//                ]
//              ]
//            ]
//          ]
//        ];

      } else {
        //记录日志
        p($res);
      }
      break;
    case "vehicleValidate":
      //请求数据
      $reqData = [
        [
          "vehicleNumber" => "闽A1234562", // 车牌号码,必填
          "vehiclePlateColorCode" => 1 // 车牌颜色,必填
        ],
        [
          "vehicleNumber" => "闽J1234563",
          "vehiclePlateColorCode" => 2
        ]
      ];
      $res = $waybill->dealReport("vehicleValidate", $reqData);
      if($res["code"] == 1){
        p($res["data"]["result"]);
        //返回数据
//        $res["data"]["result"] = [
//          [
//            "transId" => "",
//            "vehicleNumber" => "赣BTJ009", // 车牌号码
//            "vehiclePlateColorCode" => 1, // 车牌颜色
//            "validateStatus" => "通过", // 校验状态 通过，异常，待校验
//            "validateResult" => [ // 校验结果信息
//              [
//                "message" => "上报的《行驶证》必须未过期", // 校验规则信息
//                "flag" => "通过校验" // 校验规则状态 异常/通过校验
//              ]
//            ]
//          ]
//        ];
      } else {
        //记录日志
        p($res);
      }
      break;
    case "driverValidate":
      //请求数据
      $reqData = [
        [
          "drivingLicense" => "513029199908093863" // 身份证号,必填
        ],
        [
          "drivingLicense" => "513029199908093864"
        ]
      ];
      $res = $waybill->dealReport("driverValidate", $reqData);
      if($res["code"] == 1){
        p($res["data"]["result"]);

        //数据格式
//        $res["data"]["result"] = [
//          [
//            "drivingLicense" => "513029199908093863", // 身份证号
//            "validateStatus" => "通过", // 校验状态 通过，异常，待校验
//            "validateResult" => [ // 校验结果信息详情
//              [
//                "message" => "上报的《身份证》必须未过期", // 校验规则信息
//                "flag" => "通过校验" // 校验规则状态
//              ]
//            ]
//          ],
//          [
//            "drivingLicense" => "513029199908093864",
//            "validateStatus" => "异常",
//            "validateResult" => [
//              [
//                "message" => "上报的《行驶证》必须未过期",
//                "flag" => "异常"
//              ]
//            ]
//          ]
//        ];
      } else {
        //记录日志
        p($res);
      }
      break;
    case "transportAppeal":
      //请求数据
      $reqData = [
        "transportNumber" => "NO20221129000010", // 运单号,必填
        "serialNumber" => "0000", // 分段分单号,必填
        "appealContext" => "申诉申诉申诉申诉", // 申诉内容,必填
        "appealImg" => "6d04b614-2484-40f6-8a36-ebd9ed595b00.jp,b7edacc0-79b3-443d-b2e5-93bdd26a971b.jpg" // 申诉图片(多张图片用“,”分割),必填
      ];
      $res = $waybill->dealReport("transportAppeal", $reqData);
      if($res["code"] == 1){
        p($res["data"]["result"]); // 运单申诉成功
      } else {
        //记录日志
        p($res);
      }
      break;
    case "transportAppealQuery":
      //请求数据
      $reqData = [
        "transportNumber" => "NO20221129000010", // 运单号,必填
        "serialNumber" => "0000" // 分段分单号,必填
      ];
      $res = $waybill->dealReport("transportAppealQuery", $reqData);
      if($res["code"] == 1){
        p($res["data"]["result"]);

        //返回数据
        $res["data"]["result"] = [
          "appealStatus" => "2", // (申诉状态): 1-未申诉; 2-待审核; 3-审核通过; 4-审核驳回
          "appealResult" => "",
          "appealStatusName" => "待审核"
        ];
      } else {
        //记录日志
        p($res);
      }
      break;
  }
}

/**
 * 获取toke：有效期2h
 * @return array
 */
function getToken($config = []):array
{
  //暂存token_wb.json，到时改为redis存储
  $tokenFile = "./token_wb.json";
  if (file_exists($tokenFile) && $token = file_get_contents($tokenFile)) {
    if (!empty($token)) {
      return json_decode($token, true);
    }
  }

  //获取token
  $waybill = new Report($config);
  $res = $waybill->getToken();
  if($res["code"] != 1){
    //记录日志
    p($res);

    //返回
    return [
      "token" => "",
      "user" => [
        "id" => 0
      ]
    ];
  }

  //暂存token.txt，到时改为redis存储
  file_put_contents($tokenFile, json_encode($res["data"]["result"]));
  return $res["data"]["result"];
}

//配置信息
$config = [
  "baseurl" => baseurl,
  "account" => account,
  "pwd" => sha1(pwd)
];

//获取token
$token = getToken($config);
if(empty($token["token"])){
  return "token异常";
}
$config["token"] = $token;

//文件上传接口：上传电子运单、车辆信息、驾驶员信息中相关图片文件(备注:文件大小需要小于2M)
//deal("uploadFile", $config);

//电子运单上报接口：上报电子运单数据前，必须先上报通过校验的车辆数据和驾驶员数据
//deal("transport", $config);

//运单校验结果查询接口
//deal("transportValidate", $config);

//资金流水单上报接口
//deal("money", $config);

//车辆基本信息上报接口
//deal("vehicle", $config);

//车辆信息校验结果查询接口
//deal("vehicleValidate", $config);

//驾驶员基本信息上报接口
//deal("driver", $config);

//驾驶员信息校验结果查询接口
//deal("driverValidate", $config);

//运单申诉接口
//deal("transportAppeal", $config);

//运单申诉结果查询接口
//deal("transportAppealQuery", $config);